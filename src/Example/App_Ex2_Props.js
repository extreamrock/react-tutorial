import React, {Component} from 'react';

class App extends Component {
    render() {
        return (
            <div>
                <Entry label="Username" type="email" placeholder="Email"/>
                <Entry label="Password" type="password" placeholder="Password"/>
            </div>
        );
    }
}

class Entry extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <span>{this.props.label}</span>
                <input type={this.props.type} placeholder={this.props.placeholder}/>
            </div>
        );
    }
}

export default App;
