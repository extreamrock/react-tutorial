import React, {Component} from 'react';

class App extends Component {

    onClickBtn = () => {
        alert('guide')
    }

    render() {
        return (
            <div>
                <button type="button" onClick={() => alert('555')}>btn1</button>
                <button type="button" onClick={this.onClickBtn}>btn2</button>
                <MyButton onSubmit={() => {
                    alert('MyButton')
                }}/>
            </div>
        );
    }
}

class MyButton extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <button style={{backgroundColor: '#f00'}} type="button" onClick={this.props.onSubmit}>MyButton</button>
                <button type="button" class="btn btn-outline-primary">Hello World</button>
            </div>
        );
    }
}

export default App;
