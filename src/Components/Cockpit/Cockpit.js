import React, {useEffect} from "react";
import './Cockpit.css';

const Cockpit = (props) => {

    useEffect(() => {
        console.log('[Cockpit.js] useEffect');
        // Http request...
        setTimeout(() => {
            alert('Saved data to cloud!');
        }, 250);
        return () => {
            console.log('[Cockpit.js] cleanup work in useEffect');
        };
    }, []);

    useEffect(() => {
        console.log('[Cockpit.js] 2nd useEffect');
        return () => {
            console.log('[Cockpit.js] cleanup work in 2nd useEffect');
        };
    });

    const classes = []
    if (props.length <= 2) {
        classes.push('red')
    }
    if (props.length <= 1) {
        classes.pop()
        classes.push('green')
    }
    if (props.length === 0) {
        classes.pop()
        classes.push('blue')
    }

    return (
        <div>
            <h1>{props.title}</h1>
            <h2 className={classes}>{props.subTitle}</h2>
            <button onClick={props.clicked} className='btn btn-outline-primary'>{props.buttonText}</button>
        </div>
    )
}
export default React.memo(Cockpit);
