import React, {Component} from 'react';
import PropTypes from 'prop-types';

class App extends Component {
    render() {
        return (
            <div>
                {this.props.message} {this.props.count}
            </div>
        );
    }
}

App.propTypes = {
    message: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired
}

App.defaultProps = {
    message: 'guide',
    count: 1
}

export default App;

