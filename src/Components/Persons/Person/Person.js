import React from 'react';
import './Person.css';
import Aux from '../../../hoc/Auxiliary';

const Person = (props) => {
    return (
        <Aux>
            <div className="Person">
                <p onClick={props.click}>My name is {props.name} and i'm {props.age} year old</p>
                <p>{props.children}</p>
                <input type="text" onChange={props.changed} value={props.name}/>
            </div>
        </Aux>
    )
}
export default Person;
