import React, {useState} from 'react';
import Person from "../components/Persons/Person/Person";
import '../containers/App.css';

const App = props => {
    const [personsState, setPersonsState] = useState({
        person: [
            {name: 'Jesse', age: 23},
            {name: 'ROZA', age: 22},
            {name: 'Wansuk', age: 6}
        ]
    });

    const [otherState, setOtherState] = useState('Some other value')

    console.log(personsState ,otherState)

    const SwitchNameHandler = () => {
        setPersonsState({
            person: [
                {name: 'zz', age: 13},
                {name: 'xx', age: 13},
                {name: 'cc', age: 16}
            ]
        })
    }

    return (
        <div className="App">
            <h1>CONTROL</h1>
            <h2>Tokyo</h2>
            <button onClick={SwitchNameHandler} className='btn btn-primary'>Switch name</button>
            <Person name={personsState.person[0].name} age={personsState.person[0].age}>i like pizza</Person>
            <Person name={personsState.person[1].name} age={personsState.person[1].age}/>
            <Person name={personsState.person[2].name} age={personsState.person[2].age}/>
        </div>
    );

}

export default App;
