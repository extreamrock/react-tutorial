import React, {Component} from 'react';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [{
                component: 'ReactJs...',
                id: 1
            },
                {
                    component: 'Angular...',
                    id: 2
                },
                {
                    component: 'Java...',
                    id: 3
                },
                {
                    component: 'JavaScript...',
                    id: 4
                }
            ]
        }
    }

    render() {
        return (
            <div>
                {this.state.data.map((item, i) => (
                        <Content key={i} data={item}/>
                    )
                )}
            </div>
        );
    }
}

class Content extends Component {
    render() {
        return (
            <div>
                <div>{this.props.data.component}</div>
                <div>{this.props.data.id}</div>
            </div>
        );
    }
}


export default App;
