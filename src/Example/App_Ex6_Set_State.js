import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count1: 0,
            count2: 0
        };

    }

    render() {
        const {count1, count2} = this.state
        return (
            <div>
                <button onClick={() => this.setState({count1: count1 + 1})} type="button"
                        className="btn btn-outline-primary">setState V1 : {count1}</button>

                <button onClick={() => this.setState(prevState => {
                    return {count2: prevState.count1 + prevState.count2}
                })} type="button" className="btn btn-outline-primary">setState V2 : {count2}</button>

                <button onClick={() => {
                    this.forceUpdate()
                }} type="button" className="btn btn-outline-primary">forceUpdate{Math.random()}</button>

                <button onClick={() => {
                    let myDiv = document.getElementById('comment');
                    ReactDOM.findDOMNode(myDiv).style.color = 'red';
                }} type="button" className="btn btn-outline-primary">findDomNode
                </button>

                <div id='comment'><h3>guide</h3></div>
            </div>
        );
    }
}


export default App;
