import React, {Component} from 'react';
import './App.css';
import Axios from "axios";
import MovieItem from "./MovieItem";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {rows: []}
    }

    componentDidMount() {
        this.search('me')
    }

    search = (keyword) => {
        console.log(keyword)
        let movieArray = []
        let url = 'https://api.themoviedb.org/3/search/movie?api_key=d209c9d70e79868248346ee99b4a8894&query=' + keyword
        Axios.get(url).then(result => {
            console.log(result.data.results)
            result.data.results.forEach(item => {
                item.poster_src = 'https://image.tmdb.org/t/p/w185' + item.poster_path
                movieArray.push(item)
            })
            this.setState({rows: movieArray})
        })
    }

    render() {
        return (
            <div className="App">
                {/* comment */}
                <table className="Nav-bar">
                    <tbody>
                    <tr>
                        <td>
                            <img src={require('./assets/logo.svg')} width="50" alt='logo'/>
                        </td>
                        <td>
                            GUIDE React.js
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input style={{fontSize: 24, display: 'block', width: '100%', paddingLeft: 8}}
                       placeholder='Enter your movie keyword'
                       onChange={(event => {
                           this.search(event.target.value)
                       })}/>
                <div>
                    {this.state.rows.map(item => (
                        <MovieItem movie={item}/>
                    ))}
                </div>
            </div>
        );
    }
}

export default App;

