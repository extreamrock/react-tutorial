import React, {Component} from 'react';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            message: 'Reset'
        }
    }

    render() {
        return (
            <div>
                <button onClick={() => {
                    this.setState({count: this.state.count + 1})
                }} type="button" className="btn btn-outline-primary">Count: {this.state.count}</button>
                <button type="button" className="btn btn-primary" onClick={() => {
                    this.setState({count: 0, message: 'Clear'})
                }}>{this.state.message}</button>
            </div>
        );
    }
}

export default App;
