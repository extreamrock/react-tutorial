import React, {Component} from 'react';

class App extends Component {
    render() {
        return (
            <div>
                <Header></Header>
                <Content></Content>
            </div>
        );
    }
}

class Header extends Component {
    render() {
        return (
            <div style={styles.header}>
                asasd
            </div>
        );
    }
}

class Content extends Component {
    render() {
        return (
            <div style={styles.content}>
                <p>123123</p>
            </div>
        );
    }
}

const styles = {
    header: {backgroundColor: '#000', display: 'block', color: '#fff', paddingLeft: 16, height: 100},
    content: {backgroundColor: 'red', display: 'block', color: '#fff', paddingLeft: 16}
}

export default App;
