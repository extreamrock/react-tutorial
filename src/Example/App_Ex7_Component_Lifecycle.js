import React, {Component} from 'react';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {data: 0};
    }

    render() {
        return (
            <div>
                <button onClick={() => this.setState({data: this.state.data + 1})} type="button"
                        className="btn btn-outline-primary">INCREMENT
                </button>
                <Content myNumber={this.state.data}/>
            </div>
        );
    }
}

class Content extends React.Component {

    constructor(props) {
        super(props);
        console.log('constructor')
    }

    UNSAFE_componentWillMount() {
        console.log('componentWillMount')
    }

    componentDidMount() {
        console.log('componentDidMount')
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return true
    }

    UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        console.log('componentWillReceiveProps')
    }

    UNSAFE_componentWillUpdate(nextProps, nextState, nextContext) {
        console.log('componentWillUpdate')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('componentDidUpdate')
    }

    componentWillUnmount() {
        console.log('componentWillUnmount')
    }

    render() {
        return (
            <div>
                <h3>{this.props.myNumber}</h3>
            </div>
        );
    }

}

export default App;
