import React, {Component} from 'react';
import Persons from "../components/Persons/Persons";
import './App.css';
import Cockpit from '../components/Cockpit/Cockpit'

class App extends Component {

    state = {
        persons: [
            {id: '1', name: 'JAN', age: 23},
            {id: '2', name: 'GUIDE', age: 22},
            {id: '3', name: 'Wansuk', age: 6}
        ],
        otherState: 'some other value',
        showPersons: false,
        buttonText: 'SHOW',
        showCockpit: true

    }

    static getDerivedStateFromProps(props, state) {
        console.log('[App.js] getDerivedStateFromProps', props);
        return state;
    }

    componentDidMount() {
        console.log('[App.js] componentDidMount');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('[App.js] shouldComponentUpdate');
        return true;
    }

    componentDidUpdate() {
        console.log('[App.js] componentDidUpdate');
    }

    nameChangeHandler = (event, id) => {
        const personIndex = this.state.persons.findIndex(p => {
            return p.id === id
        })

        const person = {...this.state.persons[personIndex]}
        // const person = Object.assign({}, this.state.persons[personIndex])

        person.name = event.target.value

        const persons = [...this.state.persons]
        persons[personIndex] = person

        this.setState({persons: persons})
    }

    deletePersonsHandler = (personIndex) => {
        // const persons = this.state.persons
        // const persons = this.state.persons.slice()
        const persons = [...this.state.persons]
        persons.splice(personIndex, 1)
        this.setState({persons: persons})
    }

    togglePersonsHandler = () => {
        const showElement = this.state.showPersons
        this.setState({showPersons: !showElement})
        this.buttonTextHandler(showElement)
    }

    buttonTextHandler = (showElement) => {
        if (showElement) {
            this.setState({buttonText: 'SHOW'})
        } else {
            this.setState({buttonText: 'HIDE'})
        }

    }

    render() {
        console.log('[App.js] render');
        let persons = null
        if (this.state.showPersons) {
            persons = <Persons
                persons={this.state.persons}
                clicked={this.deletePersonsHandler}
                changed={this.nameChangeHandler}/>
        }
        return (
            <div className="App">
                <button onClick={() => {
                    this.setState({showCockpit: false});
                }}>Remove Cockpit
                </button>
                {this.state.showCockpit ? (

                    <Cockpit
                        title={this.props.title}
                        subTitle={this.props.subTitle}
                        showPersons={this.state.showPersons}
                        personsLength={this.state.persons.length}
                        clicked={this.togglePersonsHandler}
                        buttonText={this.state.buttonText}
                    />
                ) : null}
                {persons}
            </div>
        );
    }
}

export default App;
