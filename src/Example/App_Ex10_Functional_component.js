import React, {Component} from 'react';

class App extends Component {
    render() {
        return (
            <div>
                <Header message='1234567890'/>
                <Content title='Functional component' subtitle='Functional component'/>;
            </div>
        )
    }
}

// functional component
const Content = ({title, subtitle}) => {
    return (
        <div>
            <h2>Title: {title}</h2>
            <h3>Subtitle: {subtitle}</h3>
        </div>
    );
}

const Header = ({message}) => {
    return (
        <h1>{message}</h1>
    )
}

export default App;
